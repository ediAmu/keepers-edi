//
//  ViewController.swift
//  gpsLocation
//
//  Created by Hanan Lipskin on 10/12/2017.
//  Copyright © 2017 keepers. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
import SystemConfiguration


class ViewController: UIViewController, CLLocationManagerDelegate  {

    @IBOutlet weak var latitudeLbl: UILabel!
    @IBOutlet weak var longitudeLbl: UILabel!
    @IBOutlet weak var percentLbl: UILabel!
    @IBOutlet weak var locTimeLbl: UILabel!
    @IBOutlet weak var netActiveLbl: UILabel!
    
    
    // Used to start getting the users location
    @objc let locationManager = CLLocationManager()
    var batteryLevel:Int{return Int(UIDevice.current.batteryLevel*100)}
    var prevNetState:Bool!
    var netActive:Bool!
    
    override func viewDidLoad() {print("entered viewDidLoad")
        super.viewDidLoad()
        percentLbl.text = String(batteryLevel)
        netUpdate()
        
        // For use when the app is open & in the background
        locationManager.requestAlwaysAuthorization()
        
        //
      
        NotificationCenter.default.addObserver(self, selector: #selector(bat), name: .UIDeviceBatteryLevelDidChange, object: nil)
        
        // For use when the app is open
        //locationManager.requestWhenInUseAuthorization()
        
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {print("entered didReceiveMemoryWarning")
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Print out the location to the console
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     //print("1- \(locations)")
        if let location = locations.first {
            print("--location-- \(location.coordinate)")
            print("--batlevel--\(location.timestamp)")
            print("--netActive--\(netActive)")
            print("--batlevel--\(batteryLevel)")
            latitudeLbl.text = String(location.coordinate.latitude)
            longitudeLbl.text = String(location.coordinate.longitude)
            locTimeLbl.text = String(describing: location.timestamp)
        }
    }
    
    func netUpdate()
    {print("--entered netUpdate--")
        netActive = isConnectedToNetwork()
        if netActive != prevNetState
        {
            prevNetState = netActive
            netActiveLbl.text = String(netActive)
        }
    }
    
    @objc func bat()
    {
        percentLbl.text = String(batteryLevel)
        print("Ediii ---- \(batteryLevel)")
    }
   
    // function that returns true if conected to internet
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    /*
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to send your location we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default){ (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }*/
}

