//
//  ViewController.swift
//  backgroundChiled
//
//  Created by Hanan Lipskin on 17/12/2017.
//  Copyright © 2017 keepers. All rights reserved.
//

import UIKit
import CoreData 

class ViewController: UIViewController {

    var timer: Timer!
    
    @objc func loop()
    {
        print("This is run on the background queue")
        DispatchQueue.main.async
        {
                print("This is run on the main queue, after the previous code in outer  block")
        }
    }
    
    @IBAction func btn(_ sender: UIButton)
    {print("btn clicked")
   //     DispatchQueue.global(qos: .background).async
   //     {
            print("in async")
       //     self.loop()
            self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
            
            
    //    }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ =  UIApplication.shared.delegate as! AppDelegate
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

