//
//  ViewController.swift
//  battery
//
//  Created by Edi Amuiev on 10/12/2017.
//  Copyright © 2017 keepers. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
//    func beginBackgroundTask(withName nil: String?,
//                             expirationHandler handler: (() -> Void)? = nil) -> UIBackgroundTaskIdentifier
//    {
//        endBackgroundTask(_:)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: .UIDeviceBatteryStateDidChange, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: .UIDeviceBatteryLevelDidChange, object: nil)
        
        // get the battery level in percentes
        var batteryLevel: Int {
            return Int(UIDevice.current.batteryLevel*100)
        }
        
        let SBatt: String = "Your battery percentage is: \(batteryLevel) %"
        var batStat: String
        var batteryState: UIDeviceBatteryState {return UIDevice.current.batteryState}
        
        // show percentage
        percentageLbl.text = SBatt
        
        // change background color by the battery pecentage
        switch batteryLevel
        {
        case 80 ... 100: self.view.backgroundColor = UIColor.green
        case 50 ... 80: self.view.backgroundColor = UIColor.blue
        case 20 ... 50: self.view.backgroundColor = UIColor.yellow
        default: self.view.backgroundColor = UIColor.red
            
        }
        
        // get battery status
        switch batteryState
        {
        case .charging: batStat = "Battery charging"
        case .unknown: batStat = "?"
        case .unplugged: batStat = "Battery not charging"
        case .full: batStat = "Battery is 100% charged"
        }
        
        // show battery status
        statusLbl.text = batStat
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func batteryStateDidChange(_ notification: Notification)
    {
        var batteryState: UIDeviceBatteryState {return UIDevice.current.batteryState}
        switch batteryState {
        case .full: statusLbl.text = "Battery is 100% charged"
        case .charging: statusLbl.text = "Battery charging"
        case .unknown: statusLbl.text = "?"
        case .unplugged: statusLbl.text = "Battery not charging"
        }
        
        var batteryLevel: Int {return Int(UIDevice.current.batteryLevel*100)}
    }

    
}

