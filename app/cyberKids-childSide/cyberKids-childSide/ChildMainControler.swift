//
//  ChildMainControler.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 12/03/2018.
//  Copyright © 2018 keepers. All rights reserved.
//


import UIKit
import CoreLocation
import Alamofire
import SwiftKeychainWrapper
import SystemConfiguration.CaptiveNetwork

class ChildMainControler: UIViewController, CLLocationManagerDelegate {
    
    // net variables
    let reachability = Reachability()!
    var netConnection: String!
    
    // location variables
    let locationUtils: LocationUtils = LocationUtils.init()
    let locationManager = CLLocationManager()
    
    // battery variables
    var BatStatus: String!
    var battLevel: Int {
        get {return Int(UIDevice.current.batteryLevel*100)}
        set {}
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        var log: String = ""
        
        // check the network status
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                self.netConnection = "wifi - \(self.getWiFiSsid() ?? "")"
            } else {
                print("Reachable via Cellular") //  send to server
                self.netConnection = "Cellular"
            }
            self.sendNetworkInfo()
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n-\(Date()) --- not reachable - no enternet"
            UserDefaults.standard.set(log, forKey: "edisLog")
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        
        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SIGNEDUP)
        
        //set battery listeners
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: .UIDeviceBatteryStateDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(batteryLevelDidChange), name: .UIDeviceBatteryLevelDidChange, object: nil)
        
        // If location services is enabled check location authorization
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() { // check location authorization
            case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                locationManager.requestAlwaysAuthorization()
               
            case .authorizedAlways:
                print("'Always' location authorization is chosen")
            }
        }
        else // If location services is turned off show alert
        {
            createAlert(title: NSLocalizedString("Location services turned off", comment: "Location is off title"), message: NSLocalizedString("Please turn on your location services (GPS) in this device and try again.", comment: "Location is off body"), controller: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////      Function that called when network changes and send it to server         //////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
            self.netConnection = "wifi - \(self.getWiFiSsid() ?? "")"
        case .cellular:
            print("Reachable via Cellular")
            self.netConnection = "Cellular"
        case .none:
            print("Network not reachable")
        }
        
        self.sendNetworkInfo()
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////   Function that called when battery status or level changes and send it to server   /////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @objc func batteryLevelDidChange(_ notification: Notification){
        battLevel = Int(UIDevice.current.batteryLevel*100)
        print("batteryLevelDidChange \(battLevel)")
        
        var log: String = ""
        var auth: String!
        
        if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) == nil){
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n-\(Date()) --- battery level auth is null"
            UserDefaults.standard.set(log, forKey: "edisLog")
            print("auth is nil :(")
            return
        }
        else{
            auth = KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n\(Date()) --- battery level changed to: \(battLevel)"
            UserDefaults.standard.set(log, forKey: "edisLog")
        }
        
        let headersAuth: HTTPHeaders = ["auth": auth]
        let childId = UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!

        print("\n----- sending : \nlevel - \(battLevel), \nchildId - \(childId), \nauth - \(String(describing: auth)), \nto: \(BASE_URL_BEZEQ + CHILDREN2)/\(childId)/batterylevel?level=\(battLevel)  -----\n" )

        Alamofire.request("\(BASE_URL_BEZEQ + CHILDREN2)/\(childId)/batterylevel?level=\(battLevel)", method: .post, encoding: JSONEncoding.default, headers: headersAuth).validate(statusCode:200..<300).responseJSON {
                response in
            print("====== send battery level : \(response.result) =======")
            print(response.data?.description as Any)
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n\(Date()) --- battery level response.result \(response.result)"
            UserDefaults.standard.set(log, forKey: "edisLog")
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////   Function that called when battery status or level changes and send it to server   /////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @objc func batteryStateDidChange(_ notification: Notification){
        var batteryState: UIDeviceBatteryState {return UIDevice.current.batteryState}
        switch batteryState
        {
        case .full: BatStatus = "Battery is 100% charged"
        case .charging: BatStatus = "Battery charging"
        case .unknown: BatStatus = "?"
        case .unplugged: BatStatus = "Battery not charging"
        }
        
        print("batteryStateDidChange : \(BatStatus)")
        
        var log: String = ""
        var auth: String!
        
        if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) == nil){
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n-\(Date()) --- battery status auth is null"
            UserDefaults.standard.set(log, forKey: "edisLog")
            print("auth is nil :(")
            return
        }
        else{
            auth = KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n\(Date()) --- battery status changed to: \(BatStatus)"
            UserDefaults.standard.set(log, forKey: "edisLog")
        }
        
        let headersAuth: HTTPHeaders = ["auth": auth]
        let childId = UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!
        let parameters: Parameters = ["childId": childId, "eventType": "batteryStatus", "eventData": BatStatus]
        
        print("\n----- sending : \nchildId - \(childId), \nauth - \(String(describing: auth)), \neventType - batteryStatus, \neventData - \(BatStatus), \nto - \(BASE_URL_BEZEQ)childEvents/addForChild/ -----\n" )
        
        Alamofire.request(BASE_URL_BEZEQ + "childEvents/addForChild/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headersAuth).validate(statusCode:200..<300).responseJSON
            {
                response in
                print("====== send battery percrntage : \(response.result) =======")
                print(response.data?.description as Any)
                log = UserDefaults.standard.string(forKey: "edisLog")!
                log += "\n\(Date()) --- battery response.result \(response.result)"
                UserDefaults.standard.set(log, forKey: "edisLog")
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////          Function that get the conected to Wi-Fi name           ///////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func getWiFiSsid() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////          Function that send the network information to the server           ///////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func sendNetworkInfo(){
        print("--- Entered sendNetworkInfo ---")
        
        var log: String = ""
        var auth: String!
//        var lastNetDat: Date!

//        if(lastNetDat == nil){
//            lastNetDat = Date()
//        }
        
//        else if(timeInterval == nil || timeInterval > 900.00){){
//
//        }
        
        log = UserDefaults.standard.string(forKey: "edisLog")!
        log += "\n-\(Date()) --- network: \(self.netConnection)"
        UserDefaults.standard.set(log, forKey: "edisLog")
        
        if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) == nil){
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n-\(Date()) --- net auth is nil"
            UserDefaults.standard.set(log, forKey: "edisLog")
            print("net auth is nil :(")
            return
        }
        else{
            auth = KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)
            log = UserDefaults.standard.string(forKey: "edisLog")!
            log += "\n\(Date()) --- net auth is not nil"
            UserDefaults.standard.set(log, forKey: "edisLog")
        }
        
        let headersAuth: HTTPHeaders = ["auth": auth]
        let childId = UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!
        let parameters: Parameters = ["childId": childId, "eventType": "network", "eventData": self.netConnection]
        
        print("\n----- sending net: \nchildId - \(childId), \nauth - \(auth), \neventType - network, \neventData - \(self.netConnection), \nto - \(BASE_URL_BEZEQ)childEvents/addForChild/ -----\n" )

        Alamofire.request(BASE_URL_BEZEQ + "childEvents/addForChild/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headersAuth).validate(statusCode:200..<300).responseJSON
            {
                response in
                print("====== send network : \(response.result) --- \(String(describing: response)) =======")
                log = UserDefaults.standard.string(forKey: "edisLog")!
                log += "\n\(Date()) --- net response.result \(response.result)"
                UserDefaults.standard.set(log, forKey: "edisLog")
        }
    }
}
