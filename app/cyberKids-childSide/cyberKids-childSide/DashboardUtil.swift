//
//  DashboardUtil.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 15/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import Foundation
import UIKit

//USER DEFAULTS VALUES:
let USER_DEF_PREFIX = "keepers_user_pref_"
let USER_ID = "user_id"
let USER_AUTH = "user_auth"
let USER_PHONE_NUMBER = "user_phone_number"
let USER_EMAIL = "user_email"
let USER_NAME = "user_name"
let USER_CODE = "user_code"
let USER_SIGNEDUP = "user_signed_up"
let USER_LOGGEDIN = "user_is_logged_in"
let USER_SEEN_ONBOARDING = "user_seen_on_boarding"
let PIN_CODE_ENABLED = "PIN_CODE_ENABLED"
let PIN_CODE_CHECK = "check"

func createAlert(title: String, message: String, controller: UIViewController) {
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(
        UIAlertAction(title:
            NSLocalizedString("OK", comment: "ok option on alerts"), style: .default, handler: nil))
    controller.present(alert, animated: true, completion: nil)
}

func isValidPhoneNumber(testStr: String) -> Bool {
    
    // Match : +972 (0xx) xxxx xxx phone numbers
    let PHONE_REGEX = "^\\+{1}([9]{1})([7]{1})([2]{1})\\s\\(0\\d{2}\\)\\s\\d{4}\\s\\d{3}$"
    
    // let PHONE_REGEX = "^\\d{3}-\\d{7}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    let result =  phoneTest.evaluate(with: testStr)
    return result
}

func isValidName (testStr: String) -> Bool {

    let symbolCharacters = NSCharacterSet.symbols
    let symbolCharacters2 = CharacterSet.init(charactersIn: "!@#$%^&*()_+=-[]{}?/,.:;'|\\\"`~")
   
    let symbolRange = testStr.rangeOfCharacter(from: symbolCharacters)
    let symbolRange2 = testStr.rangeOfCharacter(from: symbolCharacters2)
    
    if symbolRange != nil || symbolRange2 != nil  {
        return false
    }
    return true
}

func isValidBirth (testStr: String) -> Int {
    
    let formatter = DateFormatter()
    formatter.dateFormat = "dd.MM.yyyy"
    let someDateTime = formatter.date(from: testStr)

    if((someDateTime?.age)! >= 18){
        return 2
    }
    else if((someDateTime?.age)! < 6){
        return 3
    }
    else{
        return 1
    }
}

struct AppUtility {
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation: UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
}

func hexStringToUIColor (hex: String) -> UIColor {
    var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if cString.hasPrefix("#") {
        cString.remove(at: cString.startIndex)
    }
    
    if (cString.count) != 6 {
        return UIColor.gray
    }
    
    var rgbValue: UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}
