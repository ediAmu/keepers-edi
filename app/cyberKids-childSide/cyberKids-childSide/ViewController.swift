//
//  ViewController.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class ViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    lazy var VCAarr: [UIViewController] = {
        return [self.VCInstance(name: "onBoarding0"),
                self.VCInstance(name: "onBoarding1"),
                //self.VCInstance(name: "onBoarding2"),
                self.VCInstance(name: "onBoarding3"),
                self.VCInstance(name: "onBoarding4"),
                self.VCInstance(name: "onBoarding5")]
    }()         //an array containing all onBoarding pages
    
    private func VCInstance(name: String ) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        if let onBoarding0 = VCAarr.first {
            setViewControllers([onBoarding0], direction: .forward, animated: true, completion: nil)
        }
        //set page Indicator UI style
        let pageControl = UIPageControl()
        UIPageControl.appearance().pageIndicatorTintColor = hexStringToUIColor(hex: "#083460")
        UIPageControl.appearance().currentPageIndicatorTintColor = hexStringToUIColor(hex: "#7E97AC")
        self.view.addSubview(pageControl)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            } else if view is UIPageControl {
                //set page Indicator background UI style
                view.backgroundColor = UIColor.clear
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //going back
        guard let viewControllerIndex = VCAarr.index(of: viewController) else {
            return nil
        }
        let prevIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left
        guard prevIndex>=0 else {
            return nil
        }
        
        guard VCAarr.count > prevIndex else {
            return nil
        }
        return VCAarr[prevIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = VCAarr.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        
        // User is on the last view controller and swiped right to loop to loginSignupPage
        guard nextIndex < VCAarr.count else {
            return nil
        }
        
        guard VCAarr.count>nextIndex else {
            return nil
        }
        return VCAarr[nextIndex]
    }
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return VCAarr.count         //amount of pages in onBoarding for pageViewController to handel
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        //what number to show
        
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = VCAarr.index(of: firstViewController) else {
                return 0
        }
        return firstViewControllerIndex
    }
}
