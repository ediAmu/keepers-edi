//
//  AppDelegate.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 05/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , CLLocationManagerDelegate, UIAlertViewDelegate{
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    let locationManagerInstance = CLLocationManager()
    var lastLocation : CLLocation!
    var lastTimeStamp : Date!
    var log: String = ""
    var locMemo : [CLLocation]!
    let reachability = Reachability()!
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIDevice.current.isBatteryMonitoringEnabled = true
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {print("WillResignActive")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        //((window?.rootViewController as? UINavigationController)?.topViewController as? ChildMainControler)?.netUpdate()
        //        ((window?.rootViewController as? UINavigationController)?.topViewController as? ChildMainControler)?.bat()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {print("DidEnterBackground")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {print("WillEnterForeground")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {print("-- did become active --")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        let arcLocations = NSKeyedArchiver.archivedData(withRootObject: locMemo ?? NSArray())
        UserDefaults.standard.set(arcLocations, forKey: "Locs")
        
        self.locationManagerInstance.startMonitoringSignificantLocationChanges()
        log = UserDefaults.standard.string(forKey: "edisLog")!
        log += "\n \(Date()) --- applicationWillTerminate"
        UserDefaults.standard.set(log, forKey: "edisLog")
    }
    
    // listener to location authorization
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        initiateUpdatingRequestOrRequestLocation(status)
    }
    
    // if location authorization changed and is not '.authorizedAlways' ask to change it
    fileprivate func initiateUpdatingRequestOrRequestLocation(_ status: CLAuthorizationStatus) {
        print("initiateUpdatingRequestOrRequestLocation in AppDelegate")
        DispatchQueue.main.async {
            
            if status == .notDetermined{
                return
            }else if status != .authorizedAlways {
                self.askToChangeRemotetlyNotification()
            }else{
                self.initLocationListining()
                self.addSettingsChangeObserver()
            }
            if !CLLocationManager.significantLocationChangeMonitoringAvailable(){
                LocationUtils.init().setupLocationManager()
            }
        }
    }
    
    fileprivate func addSettingsChangeObserver() -> Void{
        NotificationCenter.default.addObserver(self, selector: #selector(self.defaultChanged), name: UserDefaults.didChangeNotification,                                  object: nil)
        
    }
    
    func initLocationListining() -> Void {
        //        locationManagerInstance.requestAlwaysAuthorization() // -- enable when skeepink registration --
        locationManagerInstance.delegate = self
        locationManagerInstance.desiredAccuracy = kCLLocationAccuracyBest
        locationManagerInstance.distanceFilter = 25
        locationManagerInstance.allowsBackgroundLocationUpdates = true
        locationManagerInstance.pausesLocationUpdatesAutomatically = false
        locationManagerInstance.stopMonitoringSignificantLocationChanges()
        locationManagerInstance.startUpdatingLocation()
    }
    
    @objc func defaultChanged(_ notification: Notification) {
//        if let userDefaults = notification.object{
//            print("data got change in \(userDefaults)")
//        }
    }
    
    func askToChangeRemotetlyNotification() -> Void{
        //        TODO either show the GIF or
        let alertController = UIAlertController(title: "Location Services is unauthorized", message: "In order to use Keepers you need to activate 'Alow Always'", preferredStyle: .alert)
        let goToLocationSettingsAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default){ (action) -> Void in
            self.goToLocationSettings()
        }
        alertController.addAction(goToLocationSettingsAction)
        let topWindow = UIWindow.init(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController.init()
        topWindow.windowLevel = UIWindowLevelAlert + 1
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func goToLocationSettings() -> Void{
        UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("stopped location updates")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("-- received error when was receiving location changes : \(error) --")
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("-- resumed location updates --")
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////    Function that activated when locations changes (receiving new location/s)    ////////////
    ////////////////////////          and save them to userDefauls variable           ////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("\n\n--- entered locationManager ----")
        
//        UserDefaults.standard.set("", forKey: "edisLog") // Delete the log
//        UserDefaults.standard.removeObject(forKey: "Locs")// remove the "Locs" from "UserDefaults"
//        UserDefaults.standard.removeObject(forKey: "help")// remove the "help" from "UserDefaults"
        
        
        //if log variable is not exist - create it
        if(UserDefaults.standard.object(forKey: "edisLog") == nil){
            UserDefaults.standard.set("", forKey: "edisLog")
        }
        
        //if location local (colection) variable is not exist - create it
        if(UserDefaults.standard.object(forKey: "Locs") == nil && UserDefaults.standard.object(forKey: "Locs") as? Data == nil){
            print("\nlocs is nil")
            let arcLocations = NSKeyedArchiver.archivedData(withRootObject: locations)
            UserDefaults.standard.set(arcLocations, forKey: "Locs")
        }
        else{
//            print("\nlocs is not nil")
            locMemo = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey:"Locs")! as! Data) as! [CLLocation]
            locMemo = locMemo + locations
//            for locM in locMemo{
//                print("\nlocs: \(locM)")
//            }
            let arcLocations = NSKeyedArchiver.archivedData(withRootObject: locMemo ?? NSArray())
            UserDefaults.standard.set(arcLocations, forKey: "Locs")
        }
       
//        if(UserDefaults.standard.object(forKey: "help") == nil){
//            let arHelp = NSKeyedArchiver.archivedData(withRootObject: locations)
//            UserDefaults.standard.set(arHelp, forKey: "help")
//        }

        var log: String = ""
        log = UserDefaults.standard.string(forKey: "edisLog")!
        log += "\n\(Date()) --- locations.count: \(locations.count)"
        UserDefaults.standard.set(log, forKey: "edisLog")
        
        
        print("\nlocations length : \(locations.count)")
//        print("\n_+_ locations : \(locations) _+_")
//        print("\n--- locations.first : \(String(describing: locations.first)) _+_")
        

        // if there is an internet connection - send location to server
        if(isConnectedToNetwork()){
            self.sendLocation()
        }
        
//        print("\n-- location -- \(String(describing: manager.location?.coordinate)) --")
//        print("-- timestamp -- \(String(describing: manager.location?.timestamp)) --")
//        print("-- speed -- \(String(describing: manager.location?.speed)) --\n")
//
//        // get address from location
//        let ceo: CLGeocoder = CLGeocoder()
//        let loc: CLLocation = CLLocation(latitude: (manager.location?.coordinate.latitude)!, longitude: (manager.location?.coordinate.longitude)!)
//        var distanceMeters: Int!
//        var timeInterval: TimeInterval!
//        var newTimeStemp: Date!
//        newTimeStemp = manager.location?.timestamp
//        var addressString : String = ""
//
//        if(lastLocation != nil){
//            distanceMeters = Int(loc.distance(from: lastLocation))
//            lastLocation = loc
//            print("distanceMeters: \(distanceMeters)")
//        }
//        else{
//            lastLocation = loc
//        }
//
//        if(lastTimeStamp != nil){
//            timeInterval = newTimeStemp.timeIntervalSince(lastTimeStamp)
//            lastTimeStamp = newTimeStemp
//            print("timeInterval: \(timeInterval)")
//        }
//        else{
//            lastTimeStamp = newTimeStemp
//        }
//
//        // check if distance from previos location is larger than 24 meters or time interval from previos timeStamp is larger than 15 minuts
//        if(distanceMeters == nil || distanceMeters > 24 || timeInterval == nil || timeInterval > 900.00){
//            ceo.reverseGeocodeLocation(loc, completionHandler:
//                {(placemarks, error) in
//                    if (error != nil)
//                    {
//                        print("reverse geodcode fail: \(error!.localizedDescription)")
//                        log = UserDefaults.standard.string(forKey: "edisLog")!
//                        log += "\n\(Date()) --- err - geodcoder : \(error!.localizedDescription)"
//                        UserDefaults.standard.set(log, forKey: "edisLog")
//                    }
//
//                    else if (placemarks == nil)
//                    {
//                        print("placemarks is nil")
//                        log = UserDefaults.standard.string(forKey: "edisLog")!
//                        log += "\n-\(Date()) --- placemarks is nil"
//                        UserDefaults.standard.set(log, forKey: "edisLog")
//                        return
//                    }
//
//                    let pm = placemarks! as [CLPlacemark]
//
//                    if pm.count > 0 {
//                        let pm = placemarks![0]
//
//                        if pm.subLocality != nil {
//                            addressString += pm.subLocality! + ", "
//                        }
//                        if pm.thoroughfare != nil {
//                            addressString += pm.thoroughfare! + ", "
//                        }
//                        if pm.locality != nil {
//                            addressString += pm.locality! + ", "
//                        }
//                        if pm.country != nil {
//                            addressString += pm.country! + ", "
//                        }
//                        if pm.postalCode != nil {
//                            addressString += pm.postalCode! + " "
//                        }
//                        if pm.subThoroughfare != nil {
//                            addressString += pm.subThoroughfare! + " "
//                        }
//                        print("-- address : \(addressString) --")
//                    }
//
//                    //                print("----- locationData \(locationData) ------")
//
//                    var log: String = ""
//                    var auth: String!
//
//
//                    if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) == nil){
//                        log = UserDefaults.standard.string(forKey: "edisLog")!
//                        log += "\n-\(Date()) --- location auth is null"
//                        UserDefaults.standard.set(log, forKey: "edisLog")
//                        print("auth is nil :(")
//                        return
//                    }
//                    else{
//                        auth = KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)
//                        log = UserDefaults.standard.string(forKey: "edisLog")!
//                        log += "\n\(Date()) --- location auth is not null"
//                        UserDefaults.standard.set(log, forKey: "edisLog")
//                    }
//
//                    let headersAuth: HTTPHeaders = ["auth": auth!]
//                    let childId = UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!
//                    let parameters: Parameters = ["childId": childId, "latitude": manager.location?.coordinate.latitude as Any, "longitude": manager.location?.coordinate.longitude as Any, "address": addressString, "dateCreated": Int((manager.location?.timestamp.timeIntervalSince1970)!), "speed": manager.location?.speed as Any]
//
//                    print("\n----- sending : \nchildId - \(childId), \nauth - \(auth), \nlatitude - \(String(describing: manager.location?.coordinate.latitude)), \nlongitude - \(String(describing: manager.location?.coordinate.longitude)), \ndateCreated - \(Int((manager.location?.timestamp.timeIntervalSince1970)!)) \naddress - \(addressString) \naddress - \(String(describing: manager.location?.speed)) \nto - \(BASE_URL_BEZEQ)location/addForChild/ -----\n" )
//
//
//                    Alamofire.request(BASE_URL_BEZEQ + "location/addForChild/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headersAuth).validate(statusCode:200..<300).responseJSON
//                        {
//                            response in
//                            print("====== send location : \(response.result) --- \(String(describing: response)) =======")
//                            log = UserDefaults.standard.string(forKey: "edisLog")!
//                            log += "\n\(Date()) --- location response.result \(response.result)"
//                            UserDefaults.standard.set(log, forKey: "edisLog")
//                    }
//            })
//        }
        
        
        //        TODO here you receive the location either from significantLocationChangeUpdate or regular.
        //        while the app is open its using the regular change listening every 10 minutes.
        //        the significant its either it changes Cellular Antena or every 500 meteters or so.
        //        good luck. if you need anything feel free to call
        //        PS don't forget to change the text in the PLIST for all locations requests. those text will appear when you request the access to the location
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////    private function that sends the locations from UserDefoults variable to server    //////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    fileprivate func sendLocation() -> Void{
   
        var log: String = ""
        var locToSend: [CLLocation]!
        var locToSave: [CLLocation]!
        
        //if location UserDefaults variable (colection) variable is not exist
        if(UserDefaults.standard.object(forKey: "Locs") == nil && UserDefaults.standard.object(forKey: "Locs") as? Data == nil){
        //    print("\nlocs is nil")
            return
        }
        else{
        //    print("\nlocs is not nil")
            locToSend = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey:"Locs")! as! Data) as! [CLLocation]
            //UserDefaults.standard.removeObject(forKey:"Locs")
            
            for locM in locToSend{
                
                // check if there is still inernet connection - if not save to userDefaults variable end exit the function
                if(!isConnectedToNetwork()){
                    
                    var locationsLeft = locToSend[locToSend.index(of: locM)!..<locToSend.count]
                    if(UserDefaults.standard.object(forKey:"Locs") != nil){
                        locToSave = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey:"Locs")! as! Data) as! [CLLocation]
                        locationsLeft = locationsLeft + Array(locToSave)
                    }
                        
                        let arcLocations = NSKeyedArchiver.archivedData(withRootObject: locationsLeft)
                        UserDefaults.standard.set(arcLocations, forKey: "Locs")
                        
                        log = UserDefaults.standard.string(forKey: "edisLog")!
                        log += "\n \(Date()) --- have no network connection in send location loop"
                        UserDefaults.standard.set(log, forKey: "edisLog")
                        
                        return
                    
                }
                
                print("\n-- location -- \(locM.coordinate) --")
                print("-- timestamp -- \(locM.timestamp) --")
                print("-- speed -- \(locM.speed) --\n")
                
                // get address from location
                let ceo: CLGeocoder = CLGeocoder()
                let loc: CLLocation = CLLocation(latitude: (locM.coordinate.latitude), longitude: (locM.coordinate.longitude))
                var distanceMeters: Int!
                var timeInterval: TimeInterval!
                let newTimeStemp: Date! = locM.timestamp
                var addressString : String = ""
                
                // chek if distance and time interval between to locations is big enough
                if(lastLocation != nil){
                    distanceMeters = Int(loc.distance(from: lastLocation))
                    lastLocation = loc
                    print("distanceMeters: \(distanceMeters)")
                }
                else{
                    lastLocation = loc
                }
                
                if(lastTimeStamp != nil){
                    timeInterval = newTimeStemp.timeIntervalSince(lastTimeStamp)
                    lastTimeStamp = newTimeStemp
                    print("timeInterval: \(timeInterval)")
                }
                else{
                    lastTimeStamp = newTimeStemp
                }
                
                // check if distance from previos location is larger than 24 meters or time interval from previos timeStamp is larger than 15 minuts
                if(distanceMeters == nil || distanceMeters > 24 || timeInterval == nil || timeInterval > 900.00){
                    ceo.reverseGeocodeLocation(loc, completionHandler:
                        {(placemarks, error) in
                            // if there is an error
                            if (error != nil)
                            {
                                print("reverse geodcode fail: \(error!.localizedDescription)")
                                log = UserDefaults.standard.string(forKey: "edisLog")!
                                log += "\n\(Date()) --- err - geodcoder : \(error!.localizedDescription)"
                                UserDefaults.standard.set(log, forKey: "edisLog")
                            }
                                
                            else if (placemarks == nil){
                                print("placemarks is nil")
                                log = UserDefaults.standard.string(forKey: "edisLog")!
                                log += "\n-\(Date()) --- placemarks is nil"
                                UserDefaults.standard.set(log, forKey: "edisLog")
                            }
                            
                            else{
                                // create the addres string
                                let pm = placemarks! as [CLPlacemark]
                                
                                if pm.count > 0 {
                                    let pm = placemarks![0]
                                    
                                    if pm.subLocality != nil {
                                        addressString += pm.subLocality! + ", "
                                    }
                                    if pm.thoroughfare != nil {
                                        addressString += pm.thoroughfare! + ", "
                                    }
                                    if pm.locality != nil {
                                        addressString += pm.locality! + ", "
                                    }
                                    if pm.country != nil {
                                        addressString += pm.country! + ", "
                                    }
                                    if pm.postalCode != nil {
                                        addressString += pm.postalCode! + " "
                                    }
                                    if pm.subThoroughfare != nil {
                                        addressString += pm.subThoroughfare! + " "
                                    }
                                    print("-- address : \(addressString) --")
                                }
                            }
                            
                            
                            
                            //                print("----- locationData \(locationData) ------")
                            
                            
                            var auth: String!
                            
                            if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) == nil){
                                log = UserDefaults.standard.string(forKey: "edisLog")!
                                log += "\n-\(Date()) --- location auth is null"
                                UserDefaults.standard.set(log, forKey: "edisLog")
                                print("auth is nil :(")
                                return
                            }
                            else{
                                auth = KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)
                                log = UserDefaults.standard.string(forKey: "edisLog")!
                                log += "\n\(Date()) --- location auth is not null"
                                UserDefaults.standard.set(log, forKey: "edisLog")
                            }
                            
                            let headersAuth: HTTPHeaders = ["auth": auth!]
                            let childId = UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!
                            let parameters: Parameters = ["childId": childId, "latitude": locM.coordinate.latitude as Any, "longitude": locM.coordinate.longitude as Any, "address": addressString, "dateCreated": Int((locM.timestamp.timeIntervalSince1970)), "speed": locM.speed as Any]
                            
                            print("\n----- sending : \nchildId - \(childId), \nauth - \(auth), \nlatitude - \(locM.coordinate.latitude), \nlongitude - \(locM.coordinate.longitude), \ndateCreated - \(Int((locM.timestamp.timeIntervalSince1970))) \naddress - \(addressString) \naddress - \(locM.speed) \nto - \(BASE_URL_BEZEQ)location/addForChild/ -----\n" )
                            
                            
                            Alamofire.request(BASE_URL_BEZEQ + "location/addForChild/", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headersAuth).validate(statusCode:200..<300).responseJSON
                                {
                                    response in
                                    print("====== send location : \(response.result) --- \(String(describing: response)) =======")
                                    log = UserDefaults.standard.string(forKey: "edisLog")!
                                    log += "\n\(Date()) --- location response.result \(response.result)"
                                    UserDefaults.standard.set(log, forKey: "edisLog")
                            }
                    })
                }
                
//                 if all locations were sent - delete the userDefaults variable (remove saved locations)
                if (locM == locToSend.last){
                    usleep(2000000) // wait 2 seconds befor deleting
                    UserDefaults.standard.removeObject(forKey:"Locs")
                    log = UserDefaults.standard.string(forKey: "edisLog")!
                    log += "\n\(Date()) --- removed the userDefaults variable - 'locs'"
                    UserDefaults.standard.set(log, forKey: "edisLog")
                }
                
                usleep(10000) // wait 1/10 (tenth of a second) befor runing another loop round
            }
        }
    }
}

//extension UIWindow {
//    /// Returns the currently visible view controller if any reachable within the window.
//    public var visibleViewController: UIViewController? {
//        return UIWindow.visibleViewController(from: rootViewController)
//    }
//    public static func visibleViewController(from viewController: UIViewController?) -> UIViewController? {
//        switch viewController {
//        case let navigationController as UINavigationController:
//            return UIWindow.visibleViewController(from: navigationController.visibleViewController ?? navigationController.topViewController)
//        case let tabBarController as UITabBarController:
//            return UIWindow.visibleViewController(from: tabBarController.selectedViewController)
//        case let presentingViewController where viewController?.presentedViewController != nil:
//            return UIWindow.visibleViewController(from: presentingViewController?.presentedViewController)
//        default:
//            return viewController
//        }
//    }
//}

