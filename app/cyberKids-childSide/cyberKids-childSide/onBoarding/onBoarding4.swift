//
//  onBoarding4.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class onBoarding4: UIViewController {
    
    @IBOutlet weak var onBoardingTitle: UILabel!
    @IBOutlet weak var onBoardingImg: UIImageView!
    @IBOutlet weak var onBoardingTxt: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipWidth: NSLayoutConstraint!
    
    var counter = 0
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        onBoardingTxt.text = NSLocalizedString("• Battery level: Monitor your child's battery level on their phone and get an alert if the battery is under 10%.", comment: "onBoarding4 subtitle")
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            skipButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            skipButton.imageEdgeInsets.bottom = 4
            //skipButton.imageEdgeInsets.right = 5
            
            //"start" title instead of "skip". this is the last onboarding
            skipButton.imageEdgeInsets.right = -2
        } else {
            //"start" title instead of "skip". this is the last onboarding
            /*if( Locale.preferredLanguages[0].contains("de")) {
             skipWidth.constant = 150
             self.view.updateConstraints()
             }
             else */if ( Locale.preferredLanguages[0].contains("vi")) {
                skipWidth.constant = 100
                self.view.updateConstraints()
             } else if (Locale.preferredLanguages[0].contains("it")) {
                skipWidth.constant = 120
                self.view.updateConstraints()
            }
            
        }
        timer = Timer.scheduledTimer(timeInterval: 0.50, target: self, selector: #selector(onBoarding4.animate), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func animate() {
        onBoardingImg.image = UIImage(named: "battery_frame_\(counter)")
        counter += 1
        if (counter == 10) {            //end of animation
            counter = 0                 //reset
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
