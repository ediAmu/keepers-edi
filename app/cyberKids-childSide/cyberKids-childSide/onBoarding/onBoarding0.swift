//
//  onBoarding0.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class onBoarding0: UIViewController {
    
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var letsGoBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bubbleImg: UIImageView!
    @IBOutlet weak var logoImg: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = NSLocalizedString("Cyber Kids detecting suspicious and abusive messages on your child's smartphone and alerting you in real-time.", comment: "onBoarding0 title")
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            backgroundImg.transform = CGAffineTransform(scaleX: -1, y: 1)
            bubbleImg.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
