//
//  onBoarding5.swift
//  cyberKids-childSide
//
//  Created by HEduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class onBoarding5: UIViewController {
    
    @IBOutlet weak var startWidth: NSLayoutConstraint!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var onBoardingImg: UIImageView!
    @IBOutlet weak var onBoardingTitle: UILabel!
    @IBOutlet weak var onBoardingTxt: UILabel!
    var counter = 0
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        onBoardingTxt.text = NSLocalizedString("• Concealed app on the child's device.\n• Uninstall proof protection.", comment: "onBoarding5 subtitle")
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            startBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            startBtn.imageEdgeInsets.bottom = 4
            startBtn.imageEdgeInsets.right = -2
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1.20, target: self, selector: #selector(onBoarding5.animate), userInfo: nil, repeats: true)            //start animation
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func animate() {
        onBoardingImg.image = UIImage(named: "lock_frame_\(counter)")
        counter += 1
        if (counter == 2) {            //end of animation
            counter = 0                 //reset
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
