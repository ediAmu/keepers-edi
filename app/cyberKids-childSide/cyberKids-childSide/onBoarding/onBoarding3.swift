//
//  onBoarding3.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class onBoarding3: UIViewController {
    
    @IBOutlet weak var onBoardingTitle: UILabel!
    @IBOutlet weak var onBoardingImg: UIImageView!
    @IBOutlet weak var onBoardingTxt: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipWidth: NSLayoutConstraint!
    var counter = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onBoardingTxt.text = NSLocalizedString("• Tracking child's location in real time.", comment: "onBoarding3 subtitle")
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            skipButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            skipButton.imageEdgeInsets.bottom = 4
            skipButton.imageEdgeInsets.right = 5
        }
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(onBoarding3.animate), userInfo: nil, repeats: true)            //start animation
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func animate() {
        onBoardingImg.image = UIImage(named: "gps_frame_\(counter)")
        counter += 1
        if (counter == 5) {            //end of animation
            counter = 0                 //reset
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
