//
//  onBoarding1.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit

class onBoarding1: UIViewController {
    
    @IBOutlet weak var onBoardingTitle: UILabel!
    @IBOutlet weak var onBoarginImg: UIImageView!
    @IBOutlet weak var onBoardingTxt: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var skipWidth: NSLayoutConstraint!
    
    var counter = 0
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        onBoardingTxt.text = NSLocalizedString("• Media analysis technology allows to detect any alarming situation and reports it to the parent.", comment: "onBoarding1 subtitle")
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            skipButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            skipButton.imageEdgeInsets.bottom = 4
            skipButton.imageEdgeInsets.right = 5
        }
        timer = Timer.scheduledTimer(timeInterval: 0.50, target: self, selector: #selector(onBoarding1.animate), userInfo: nil, repeats: true)            //start animation
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func animate() {
        onBoarginImg.image = UIImage(named: "chat_frame_\(counter)")
        counter += 1
        if (counter == 8) {            //end of animation
            counter = 0                 //reset
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
