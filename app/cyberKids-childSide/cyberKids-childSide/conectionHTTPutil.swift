//
//  conectionHTTPutil.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 15/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import Foundation
import SystemConfiguration
import SystemConfiguration.CaptiveNetwork

let bezeq_prod     = "https://keepers-main-bezeq-prod.eu-gb.mybluemix.net/keeper-server/"
let bezeq_qa       = "https://keepers-main-bezeq-qa.eu-gb.mybluemix.net/keeper-server/"
let bezeq_dev      = "https://keepers-main-hub.eu-gb.mybluemix.net/keeper-server/"
let localServer    = "https://f1014350.ngrok.io/keeper-server/" // dudis local Server

let BASE_URL_BEZEQ  =  bezeq_dev

let PARENT = "parents"
let CHILDREN = "children"
let CHILDREN2 = "devices"

//general http utils
let HTTP_GET = "GET"
let HTTP_HEAD = "HEAD"
let HTTP_POST = "POST"
//internal error codes
let ERR_OK = "OK"
let ERR_UNEXPECTED = "UNEXPECTED"
let ERR_OK_NO_KIDS = "NO_KIDS"
let ERR_USER_ALREADY_LOGGED_IN = "USER_ALREADY_LOGGED_IN"
let ERR_USER_DOSENT_EXIST = "USER_DOSENT_EXIST"
let ERR_USER_AUTH = "ERR_USER_AUTH"
let ERR_USER_EXCEEDIG_QUOTA = "ERR_USER_EXCEEDIG_QUOTA"

//error msg from server
let INVALIDE_EMAIL = "Bad credentials invalid email"
let INVALIDE_PASSWORD = "Bad credentials invalid password"
let UNEXPECTER_ERROR = "Unexpected Error"


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////          Function that checkes if there is network connection           ////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

func isConnectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
            SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
    }) else {
        return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
}


