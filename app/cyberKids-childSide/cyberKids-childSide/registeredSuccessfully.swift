//
//  registeredSuccessfuly.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 16/04/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import CoreLocation

class registeredSuccessfully: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var titleLbl: UILabel!
    
    let locationManager = CLLocationManager()
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLbl.text = NSLocalizedString("The child registered successfuly\nPlease allow all access reqests.", comment: "child registered successfuly title")
        
        // location authorization popup requst
        if CLLocationManager.locationServicesEnabled() { // if GPS turned on
            locationManager.requestAlwaysAuthorization()
            
            self.await()
        }
        
        else // if GPS turned off show alert
        {
            createAlert(title: NSLocalizedString("Location services turned off", comment: "Location is off title"), message: NSLocalizedString("Please turn on your location services (GPS) in this device and try again.", comment: "Location is off body"), controller: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // function that waits for location authorization
    @objc func await() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
//                print("No access")
                timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.await), userInfo: nil, repeats: false)
            case .authorizedAlways, .authorizedWhenInUse:
//                print("Access")
                timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveTo), userInfo: nil, repeats: false)
            }
        } else {
            print("Location services are not enabled")
            createAlert(title: NSLocalizedString("Location services turned off", comment: "Location is off title"), message: NSLocalizedString("Please turn on your location services (GPS) in this device and try again.", comment: "Location is off body"), controller: self)
        }
    }
    
    @objc func moveTo(){
//        print("------ move to -------")
        performSegue(withIdentifier: "sucRegToComplete", sender: self)
    }

}
