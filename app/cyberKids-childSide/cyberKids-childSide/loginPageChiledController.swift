//
//  loginPageChildController.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 18/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftKeychainWrapper

class loginPageChildController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var welcomTitle: UILabel!
    @IBOutlet weak var lablePhone: UILabel!
    @IBOutlet weak var labelChildName: UILabel!
    @IBOutlet weak var inputChildName: UITextField!
    @IBOutlet weak var ActivityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var childFieldsWrapper: UIView!
    @IBOutlet weak var lineName: UIView!
    @IBOutlet weak var lineDate: UIView!
    @IBOutlet weak var linePhone: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var inputPhoneNumber: UITextField!
    @IBOutlet weak var inputDate: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    
    var USER_ALREDY_EXISTS: Bool = false
    var moveableInfo = [String: Any?] ()
    var isEyeOpen = false
    var virginPhoneNumber = true
    var virginName = true
    var virginDate = true
    var timer = Timer()
    
    let PHONE_NUMBER_STATIC_PREFIX = "+972 (0"
    let picker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        setupInputDeleagates()
        addToolBarToTextField(textField: inputPhoneNumber)
        inputPhoneNumber.keyboardType = UIKeyboardType.phonePad
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            // The app is in right-to-left mode
            backBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            nextButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
        
        welcomTitle.text = NSLocalizedString("Welcome to Cyber Kids,\nPlease enter your child's name \nhis date of birth \nand your phone number.", comment: "welcome title on login page")
        
        inputPhoneNumber.textAlignment = .left
        createDatePicker()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        virginPhoneNumber = false
        virginName = false
        virginDate = false
    }
  
    func createDatePicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor.clear
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDonePressed))
        toolbar.setItems([done], animated: false)
        inputDate.inputAccessoryView = toolbar
        picker.datePickerMode = .date
        inputDate.inputView = picker
    }
    
    // date picker "Done" button behavior
    @objc func datePickerDonePressed(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let selectedDate = dateFormatter.string(from: picker.date)
        inputDate.text = selectedDate//String(describing: picker.date.)
        inputDate.resignFirstResponder()
        inputPhoneNumber.becomeFirstResponder()
    }
    
    var lastCharAmount = 0
    
    // Behavior of phone text filed
    @objc func textFieldTextChange(_ textField: UITextField) {
        if ( (inputPhoneNumber.text?.count)! > lastCharAmount ) {
            if((inputPhoneNumber.text?.count)! <  PHONE_NUMBER_STATIC_PREFIX.count  ) {
                inputPhoneNumber.text = PHONE_NUMBER_STATIC_PREFIX
            }
            if((inputPhoneNumber.text?.count)! == 9) {
                inputPhoneNumber.text = inputPhoneNumber.text! + ") "
            }
            if(inputPhoneNumber.text?.count == 10 ) {
                if( !(inputPhoneNumber.text?.suffix(1).description)!.contains(")")) {
                    var newString = inputPhoneNumber.text!
                    newString.insert(")", at: newString.index(newString.endIndex, offsetBy: -1) )
                    inputPhoneNumber.text = newString
                    
                }
            }
            if(inputPhoneNumber.text?.count == 11 ) {
                if( !(inputPhoneNumber.text?.suffix(1).description)!.contains(" ")) {
                    var newString = inputPhoneNumber.text!
                    newString.insert(" ", at: newString.index(newString.endIndex, offsetBy: -1) )
                    inputPhoneNumber.text = newString
                    
                }
            }
            if(inputPhoneNumber.text?.count == 15) {
                inputPhoneNumber.text = inputPhoneNumber.text! + " "
            }
            if(inputPhoneNumber.text?.count == 16 ) {
                if( !(inputPhoneNumber.text?.suffix(1).description)!.contains(" ")) {
                    var newString = inputPhoneNumber.text!
                    newString.insert(" ", at: newString.index(newString.endIndex, offsetBy: -1) )
                    inputPhoneNumber.text = newString
                    
                }
            }
            if(inputPhoneNumber.text?.count == 20) {
                let oldText: Substring = (inputPhoneNumber.text?.prefix(19))!
                inputPhoneNumber.text = String(describing: oldText)
            }
        }
        lastCharAmount = (inputPhoneNumber.text?.count)!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == inputPhoneNumber) {
            textField.typingAttributes = [NSAttributedStringKey.foregroundColor.rawValue:UIColor.white]
            let protectedRangePrefix = NSMakeRange(0, PHONE_NUMBER_STATIC_PREFIX.count)
            //let protectedRangeBracet = NSMakeRange(0, 11)
            let intersectionPrefix = NSIntersectionRange(protectedRangePrefix, range)
            // let intersectionBracet = NSIntersectionRange(protectedRangeBracet, range)
            //intersectionBracet.length > 0
            if intersectionPrefix.length > 0 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("func :  textFieldShouldReturn:textField:")
        textField.resignFirstResponder()
        switch textField.tag {
        case 10:
            inputDate.becomeFirstResponder()
            break
        case 11:
            inputPhoneNumber.becomeFirstResponder()
            break
        case 12:
            onClickNext(self)
            break
        default:
            print("")
        }
        return true
    }
    
    private func setupInputDeleagates() {
        inputPhoneNumber.delegate = self
        inputChildName.delegate = self
        inputPhoneNumber.addTarget(self, action: #selector(textFieldTextChange(_:)), for: .editingChanged)
        inputChildName.tag = 10
        inputDate.tag = 11
        inputPhoneNumber.tag = 12
        inputChildName.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addToolBarToTextField(textField: UITextField) {
        let numberToolbar: UIToolbar = UIToolbar()
        numberToolbar.barStyle = UIBarStyle.blackTranslucent
        numberToolbar.items=[
            UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "Cancel button on phone Number input keyboard"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(loginPageChildController.cancelPhoneNumber)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: NSLocalizedString("Done", comment: "Done button on phone Number input keyboard"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(loginPageChildController.donePhoneNumber))
        ]
        numberToolbar.sizeToFit()
        textField.inputAccessoryView = numberToolbar
    }
    
    @objc func cancelPhoneNumber () {
        inputPhoneNumber.resignFirstResponder()
        inputPhoneNumber.text = ""
        onPhoneEnd(self)
    }
    
    @objc func donePhoneNumber () {
        onClickNext(self)
        inputPhoneNumber.resignFirstResponder()
    }
    
    @IBAction func onClickNext(_ sender: Any) {
    //      start ActivityIndicatorView (spinner while loading)
        ActivityIndicatorView.startAnimating()
        self.loginValidation(phoneNumber: self.inputPhoneNumber.text!, userName: self.inputChildName.text!, birthDate: self.inputDate.text!)
    }
    
    func loginValidation(phoneNumber: String, userName: String, birthDate: String) {
        if phoneNumber=="" || userName=="" || birthDate==""
        {
            createAlert(title: NSLocalizedString("Missing fields", comment: "Missing fields alert title"), message: NSLocalizedString("Please enter your child's name, his date of birth and your phone namber and try again.", comment: "Missing fields alert body"), controller: self)
            ActivityIndicatorView.stopAnimating()
        }
        else if ( isValidPhoneNumber(testStr: phoneNumber) )
        {
            if(isValidName(testStr: userName) )
            {
                let validBD = isValidBirth(testStr: birthDate)
                switch validBD {
                case 3:
                    createAlert(title: NSLocalizedString("Invalid Birth Date", comment: "Invalid Birth Date alert title"), message: NSLocalizedString("Your child is younger than 6 years old, you can use this app for children older than 6 and younger than 18.", comment: "Invalid Name alert body"), controller: self)
                    ActivityIndicatorView.stopAnimating()
                    return
                case 2:
                    createAlert(title: NSLocalizedString("Invalid Birth Date", comment: "Invalid Birth Date alert title"), message: NSLocalizedString("Your child is older than 18 years old, you can use this app for children older than 6 and younger than 18.", comment: "Invalid Name alert body"), controller: self)
                    ActivityIndicatorView.stopAnimating()
                    return
                case 1:
                    //everything seemes legit - start handshake.
                    handshakeBezeq(phoneNumber: phoneNumber, name: userName, birthDate: birthDate)
                    return
                default:
                    createAlert(title: NSLocalizedString("Birth Date Error", comment: "Unexpected Birth Date error title"), message: NSLocalizedString("Unexpected Birth Date Error has occurred, please try again.", comment: "Invalid Name alert body"), controller: self)
                    ActivityIndicatorView.stopAnimating()
                    return
                }
            } else {
                createAlert(title: NSLocalizedString("Invalid Name", comment: "Invalid Name alert title"), message: NSLocalizedString("The name you entered contains simbols, remove them and please try again.", comment: "Invalid Name alert body"), controller: self)
                ActivityIndicatorView.stopAnimating()
            }
        }
        else {
            createAlert(title: NSLocalizedString("Invalid Phone Number", comment: "Invalid Phone Number alert title"), message: NSLocalizedString("Something is wrong with your phone number, please try again.", comment: "Invalid Phone Number alert body"), controller: self)
            ActivityIndicatorView.stopAnimating()
        }
    }
    
    func handshakeBezeq ( phoneNumber: String, name: String, birthDate: String) {
        //cleanUp phone Number :
        var newString = phoneNumber.replacingOccurrences(of: " ", with: "")
        newString = newString.replacingOccurrences(of: "(", with: "")
        newString = newString.replacingOccurrences(of: ")", with: "")
        newString = newString.replacingOccurrences(of: "+", with: "")

        var errorCode: String = ERR_OK
        var userInfo: NSDictionary = NSDictionary()
// --------- set the right date format for server ---------------
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YYYY"
        
        let showDate = dateFormatter.date(from: birthDate)
        dateFormatter.dateFormat = "YYYY-MM-dd"
        
        let resultDate = dateFormatter.string(from: showDate!)
        
// --------- Create new child for registered parent ----------
        let parameters: Parameters = ["name": name, "dateOfBirth": resultDate]
        print("\n-----sending : phoneNumber \(newString) , name \(name) , dateOfBirth: \(resultDate) . to : " + BASE_URL_BEZEQ + CHILDREN + "/createAndRegisterPhone/ -----\n" )

        Alamofire.request(BASE_URL_BEZEQ + CHILDREN + "/createAndRegisterPhone/" + newString, method : .post, parameters : parameters, encoding : JSONEncoding.default).validate(statusCode: 200..<300).responseJSON//
            {
                response in

//                print("\nresponse.description: \(response.description)\n")
//                print("\nresponse.value: \(String(describing: response.value))\n")
                print("\nresponse.result: \(response.result)\n")
//                print("\n------------------ Full response: ------------------\n \(response)\n")


                switch response.result {
                    //---- bezeq created a user (child) account ----
                    case .success:
                        if let json = response.result.value {
//                            print("JSON: \(json)") // serialized json response
                            userInfo = json as! NSDictionary
                            self.handshakeBezeqComplitionHendler(errorCode: errorCode, userInfo: userInfo, phoneNumber: newString)
                            return

                        }

                    //---- bezeq didn't create a user account ----
                    case .failure:
                        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                            print("Data: \(utf8Text)") // original server data as UTF8 string

                            //bezeq didn't create a user account
                            //parent have max amount of loged in users!
                            if(utf8Text.contains("999") || utf8Text.contains("Parent's child registration is exceeding quota")) {
                                errorCode = ERR_USER_EXCEEDIG_QUOTA
                                print("\n----Parent can't have more loged in kids-----\n")
                            }
                            //parent was not found
                            else if (utf8Text.contains("994") || utf8Text.contains("Requested parent was not found")){
                                errorCode = ERR_USER_DOSENT_EXIST
                                print("\n----Parent was not found-----\n")
                            }
                            else if(utf8Text.contains("Unexpected Error")) {errorCode = ERR_UNEXPECTED}
                            else if(utf8Text.contains("Requested parent was not found")) {errorCode = ERR_USER_DOSENT_EXIST}
                            else if(utf8Text.contains("Parent account is inactive")) {errorCode = ERR_USER_DOSENT_EXIST}
                            if( errorCode != ERR_OK)
                            {
                                self.handshakeBezeqComplitionHendler(errorCode: errorCode, userInfo: nil, phoneNumber: "")
                                print("--- error 1 creating new child user ---")
                                return
                            }
                        }
                }
        }        
    }
    
    //end handshakeBezeq
    func handshakeBezeqComplitionHendler(errorCode: String, userInfo: NSDictionary?, phoneNumber: String) {
        ActivityIndicatorView.stopAnimating()
        print("errorCode: " + errorCode)
        if(userInfo != nil && phoneNumber != "" && errorCode == ERR_OK) {
            print("HandshakeCompHendler: status- OK")
//            addShortcutItems(isLoggedIn: true)
            let userProfile: NSDictionary = userInfo!
//            if(userInfo!["parent"] != nil) {
//                userProfile = userInfo!.value(forKey: "parent") as! NSDictionary
//            }
            UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_LOGGEDIN)
            UserDefaults.standard.set(phoneNumber, forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)
            
            //KeychainWrapper variable do not removed with application removal so if there old auth still saved remove it.
            if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) != nil){
                KeychainWrapper.standard.removeObject(forKey: USER_DEF_PREFIX + USER_AUTH)
            }
            KeychainWrapper.standard.set(userProfile["authToken"] as! String, forKey: USER_DEF_PREFIX + USER_AUTH, withAccessibility: .always)
            //UserDefaults.standard.set(userProfile["removalDialCode"]!, forKey: USER_DEF_PREFIX + USER_CODE)
            UserDefaults.standard.set(userProfile["id"] as! Int, forKey: USER_DEF_PREFIX + USER_ID)
            UserDefaults.standard.set(userProfile["name"], forKey: USER_DEF_PREFIX + USER_NAME)

            print("\n -------- UserDefaults -----------\n")
            print("phoneNumber: " + UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)!)
            //print("removalDialCode: " + UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_CODE)!)
            print("id: " + UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_ID)!)
            print("name: " + UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_NAME)!)
            print("authToken: " + KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)!)
            print("\n ---------------------------------\n")
            print("\n Befor segue\n")
            performSegue(withIdentifier: "childLoginToSucReg", sender: self)
        }
        else {
            switch errorCode {
            case ERR_USER_DOSENT_EXIST :
                createAlert(title: NSLocalizedString("Internal Error", comment: "general error title"), message: NSLocalizedString("Sorry, This phone number doesn't exist in our system or have been disactivated, Please contact Bezeq to register it", comment: "general error body"), controller: self)
            case ERR_UNEXPECTED :
                createAlert(title: NSLocalizedString("Internal Error", comment: "general error title"), message: NSLocalizedString("Sorry, something went wrong on our side, please try again", comment: "general error body"), controller: self)
            case ERR_USER_EXCEEDIG_QUOTA :
                createAlert(title: NSLocalizedString("Internal Error", comment: "general error title"), message: NSLocalizedString("Sorry, Your amount of child users is exeeded quota, Please contact Bezeq for more information", comment: "general error body"), controller: self)
////            case ERR_USER_ALREADY_LOGGED_IN:
//                USER_ALREDY_EXISTS = true
//                performSegue(withIdentifier: "LoginToContact", sender: self)
//            case ERR_USER_DOSENT_EXIST:
//                USER_ALREDY_EXISTS = false
//                print("HandshakeCompHendler: status-" + ERR_USER_DOSENT_EXIST)
//                performSegue(withIdentifier: "LoginToContact", sender: self)
//
//                Mixpanel.mainInstance().identify(distinctId: emailStr)
//                Mixpanel.mainInstance().track(event: SIGN_UP_FROM_PARENT_DEVICE_EVENT)
//                self.performSegue(withIdentifier: "signupToPairing", sender: self)
            default:
                print("HandshakeCompHendler: status- " + errorCode)
            }
        }
        
    }
    
    //animate Name label on first touch
    @IBAction func onFirstTouchName(_ sender: Any) {
        if( virginName  ) {
            moveComponent(labelChildName, relativeTo: inputChildName, location: .top)
            virginName = false
        }
    }
    
    //animate Name label on End edit if empty
    @IBAction func onNameEnd(_ sender: Any) {
        if(inputChildName.text == "")
        {
            moveComponent(labelChildName, relativeTo: inputChildName, location: .bottom, constant: -30.0)
            virginName = true
        }
    }
    
    //animate Date label on first touch
    @IBAction func onFirstTouchDate(_ sender: Any) {
        if(virginDate) {
            moveComponent(dateLabel, relativeTo: inputDate, location: .top)
            virginDate = false
        }
    }
    
    //animate Date label on End edit if empty
    @IBAction func onDateEnd(_ sender: Any) {
        if(inputDate.text == "")
        {
            moveComponent(dateLabel, relativeTo: inputDate, location: .bottom, constant: -30.0)
            virginDate = true
        }
    }

    //animate Phone label on first touch
    @IBAction func onFirstTouchPhone(_ sender: Any) {
        scroll.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
        if(virginPhoneNumber) {
            moveComponent(lablePhone, relativeTo: inputPhoneNumber, location: .top)
            virginPhoneNumber = false
            
            let attributedString = NSMutableAttributedString(string: PHONE_NUMBER_STATIC_PREFIX)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSMakeRange(0, PHONE_NUMBER_STATIC_PREFIX.count))
            inputPhoneNumber.attributedText = attributedString
        }
    }
    
    //animate Phone label on End edit if empty
    @IBAction func onPhoneEnd(_ sender: Any) {
        if(inputPhoneNumber.text == "")
        {
            moveComponent(lablePhone, relativeTo: inputPhoneNumber, location: .bottom, constant: -30.0)
            virginPhoneNumber = true
        }
        else if(inputPhoneNumber.text == PHONE_NUMBER_STATIC_PREFIX)
        {
            inputPhoneNumber.text = ""
            moveComponent(lablePhone, relativeTo: inputPhoneNumber, location: .bottom, constant: -30.0)
            virginPhoneNumber = true
        }
    }
    
    enum RelativeLocation {
        case top
        case bottom
        case right
        case left
    }
    
    private func moveComponent(_ component: UIView, relativeTo view: UIView, location: RelativeLocation, constant: CGFloat) {
        
        var relativeYOffset: CGFloat = 0.0
        var relativeXOffset: CGFloat = 0.0
        switch location {
        case .top:
            relativeYOffset = -(view.frame.size.height)
            relativeYOffset += constant
        case .bottom:
            relativeYOffset = view.frame.size.height
            relativeYOffset += constant
        case .right:
            if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
                relativeXOffset = -(view.frame.size.width)
            } else {
                relativeXOffset = view.frame.size.width
                
            }
            relativeXOffset += constant
        case .left:
            if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
                relativeXOffset = view.frame.size.width
            } else {
                relativeXOffset = -(view.frame.size.width)
                
            }
            relativeXOffset += constant
        }
        
        UIView.animate(withDuration: 0.5) {
            component.layer.transform = CATransform3DMakeTranslation(relativeXOffset, relativeYOffset, 0)
            
        }
    }

    private func moveComponent(_ component: UIView, relativeTo view: UIView, location: RelativeLocation) {
        moveComponent(component, relativeTo: view, location: location, constant: 0)
    }
    
    private func moveComponent(_ components: [UIView], relativeTo view: UIView, location: RelativeLocation) {
        for component in components {
            moveComponent(component, relativeTo: view, location: location, constant: 0)
        }
    }
    
    private func moveComponent(_ components: [UIView], relativeTo view: UIView, location: RelativeLocation, constant: CGFloat) {
        for component in components {
            moveComponent(component, relativeTo: view, location: location, constant: constant)
        }
    }
}

extension UITextField {
    func setCursor(position: Int) {
        let position = self.position(from: beginningOfDocument, offset: position)!
        selectedTextRange = textRange(from: position, to: position)
    }
}
