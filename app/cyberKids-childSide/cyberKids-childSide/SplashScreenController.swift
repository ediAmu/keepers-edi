//
//  SplashScreenController.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 05/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import CoreLocation

class SplashScreenController: UIViewController {

    var counter = 1
    var timer = Timer()
    var timer2 = Timer()
    var didPassEnded = false
    
    
    @IBOutlet weak var keepersLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var splashLblToStackMin: NSLayoutConstraint!
    
    
    func moveto() {
        if(UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_LOGGEDIN) != nil ) && (UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_LOGGEDIN) as! Bool == true) {
            print("user is logged in ")
            
            print(UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_ID) ?? "-- ID is null - in splash --")
            print(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) ?? "-- Auth is null - in splash --")
            
            if(KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH) != nil &&
                KeychainWrapper.standard.string(forKey: USER_DEF_PREFIX + USER_AUTH)! != "" &&
                UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_ID) != nil  ) {
                print("user Auth && ID are valid ")
//                print("USER_ID - \(String(describing: UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_ID)))")
//                print("user name - \(String(describing: UserDefaults.standard.string(forKey: USER_DEF_PREFIX + USER_NAME))) ")
                self.performSegue(withIdentifier: "splashToLoginComplete", sender: self)
                return
            }
        }

        else{
            print("user isn't logged in")
        
            if(UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_SEEN_ONBOARDING) != nil ) &&
                (UserDefaults.standard.value(forKey: USER_DEF_PREFIX+USER_SEEN_ONBOARDING) as! Bool == true) {
//                print("user didn't signUp but seen onBoarding")
                //goto Parent/child
                self.performSegue(withIdentifier: "splashToParentChild", sender: self)
                return
            }
            
            else{//goto onBoarding
//                print("user didn't see onBoarding")
                self.performSegue(withIdentifier: "splashToOnBoarding", sender: self)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).initLocationListining()
        
        // my log for development
        if(UserDefaults.standard.string(forKey: "edisLog") == nil){
            UserDefaults.standard.set("", forKey: "edisLog")
        }
        else{
//            UserDefaults.standard.set("", forKey: "edisLog") // Delete the log
            print("------- edislog: ---------\n\(UserDefaults.standard.string(forKey: "edisLog")!)\n--------------------------------------\n")
        }
        
//        UserDefaults.standard.removeObject(forKey: "Locs")// remove the "Locs" from "UserDefaults"
        
        if(UserDefaults.standard.object(forKey: "Locs") == nil && UserDefaults.standard.object(forKey: "Locs") as? Data == nil){
            print("-- SplasScreenController -- locs is nil ")
        }
//        else{
//            let locMemo = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey:"Locs")! as! Data) as! [CLLocation]
//            print("------- locs: ---------")
//            for locM in locMemo{
//                print("\n\(locM)")
//            }
//            print("\n--------------------------------------\n")
//        }

        
//                UserDefaults.standard.removeObject(forKey: "help")// remove the "help" from "UserDefaults"
        
//        if(UserDefaults.standard.object(forKey: "help") == nil){
//            print("-- SplasScreenController -- help is nil ")
//        }
//        else{
//            let qwqw = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey:"help")! as! Data) as! [CLLocation]
//            print("------- help: ---------")
//            for qw in qwqw{
//                print("\n\(qw)")
//            }
//            print("\n--------------------------------------\n")
//        }
        
        
                    // qa simulator
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SIGNEDUP)
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_LOGGEDIN)
//        UserDefaults.standard.set(71, forKey: USER_DEF_PREFIX + USER_ID)
//        UserDefaults.standard.set("Edi child 1", forKey: USER_DEF_PREFIX + USER_NAME)
//        UserDefaults.standard.set("9720501000000", forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)
//        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: USER_DEF_PREFIX + USER_AUTH)
//        if(removeSuccessful)
//        {
//            print("Auth removed successfully")
//        }
//        else
//        {
//            print("Auth didn't removed")
//        }
//        KeychainWrapper.standard.set("4cc7c2f8-134b-42f9-b9d9-5261c6befcd0", forKey: USER_DEF_PREFIX + USER_AUTH, withAccessibility: .always)

                    // Dev simulator
//1
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SIGNEDUP)
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_LOGGEDIN)
//        UserDefaults.standard.set(181, forKey: USER_DEF_PREFIX + USER_ID)
//        UserDefaults.standard.set("child10", forKey: USER_DEF_PREFIX + USER_NAME)
//        UserDefaults.standard.set("9720501000000", forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)
//        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: USER_DEF_PREFIX + USER_AUTH)
//        if(removeSuccessful)
//        {
//            print("Auth removed successfully")
//        }
//        else
//        {
//            print("Auth didn't removed")
//        }
//        KeychainWrapper.standard.set("a74d74ee-5016-4396-a747-d0203b833541", forKey: USER_DEF_PREFIX + USER_AUTH, withAccessibility: .always)
//2
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SIGNEDUP)
//        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_LOGGEDIN)
//        UserDefaults.standard.set(11, forKey: USER_DEF_PREFIX + USER_ID)
//        UserDefaults.standard.set("keeprs child 1", forKey: USER_DEF_PREFIX + USER_NAME)
//        UserDefaults.standard.set("9720501000000", forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)
//        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: USER_DEF_PREFIX + USER_AUTH)
//        if(removeSuccessful)
//        {
//            print("Auth removed successfully")
//        }
//        else
//        {
//            print("Auth didn't removed")
//        }
//        KeychainWrapper.standard.set("45059b58-ba25-4281-a3f0-4591f5e4633f", forKey: USER_DEF_PREFIX + USER_AUTH, withAccessibility: .always)

//3
//                UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SIGNEDUP)
//                UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_LOGGEDIN)
//                UserDefaults.standard.set(191, forKey: USER_DEF_PREFIX + USER_ID)
//                UserDefaults.standard.set("white iPhone 6", forKey: USER_DEF_PREFIX + USER_NAME)
//                UserDefaults.standard.set("9720501000000", forKey: USER_DEF_PREFIX + USER_PHONE_NUMBER)
//                let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: USER_DEF_PREFIX + USER_AUTH)
//                if(removeSuccessful)
//                {
//                    print("Auth removed successfully")
//                }
//                else
//                {
//                    print("Auth didn't removed")
//                }
//                KeychainWrapper.standard.set("10270d6c-f179-4213-b17a-03e10bd97d0a", forKey: USER_DEF_PREFIX + USER_AUTH, withAccessibility: .always)
        keepersLbl.isHidden = true     //hide text untill animation is complete
        keepersLbl.text = NSLocalizedString("Keep your child safe\non social media.", comment: "splash screen subtitle")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppUtility.lockOrientation(.all)
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        if( identifier == "iPhone10,6" || identifier == "iPhone10,3") {
            splashLblToStackMin.constant = 130
            imageView.updateConstraints()
            view.updateConstraintsIfNeeded()
        }
    }
    
//    func askPassword() {
//        if(UserDefaults.standard.value(forKey: USER_DEF_PREFIX+PIN_CODE_ENABLED) != nil) {
//            if(UserDefaults.standard.value(forKey: USER_DEF_PREFIX+PIN_CODE_ENABLED) as! Bool == true) {
////                self.performSegue(withIdentifier: "splashToSecretCode", sender: self)
//            } else {
//                self.moveto()
//            }
//        } else {
//            self.moveto()
//        }
//    }
    
    @objc func animate(){
        imageView.image =  UIImage(named: "frame_\(counter)_delay-0.03s.png")
        counter += 1
        // if got to last image in set
        if(counter == 140)
        {
            timer.invalidate()
            self.keepersLbl.isHidden = false
            timer2 = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(SplashScreenController.checkIntenetConnection), userInfo: nil, repeats: false)
        }
        
    }
    
    @objc func checkIntenetConnection() {
        //check internet connection
        if isConnectedToNetwork() == true {
            self.moveto()
            return
        } else {
            createAlert(title: NSLocalizedString("Oops!", comment: "internt connection unavilable alert title"), message: NSLocalizedString("Internet Connection not Available", comment: "internt connection unavilable alert body"), controller: self)
        }
        
        //keep waiting for internet connection , when restored go to moveto()
        DispatchQueue.global(qos: .userInitiated).async {
            // Do some time consuming task in this background thread
            // Mobile app will remain to be responsive to user actions
            while (isConnectedToNetwork() == false) {
                usleep(500000)//HALF A SECOND = 500,000 milionth of a second
            }
            DispatchQueue.main.async {
                // Task consuming task has completed
                // Update UI from this block of code
                self.moveto()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
        if (didPassEnded == true) {
            self.moveto()
            return
        }
         //start animation
        timer = Timer.scheduledTimer(timeInterval: 0.04, target: self, selector: #selector(SplashScreenController.animate), userInfo: nil, repeats: true)
    }
}
