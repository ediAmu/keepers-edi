//
//  loginSignupViewController.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 18/02/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import M13Checkbox

class loginSignupViewController: UIViewController {
    
    //define a UIWebView and set it's size to width 96% and height 94%
    let myWebView: UIWebView = UIWebView(frame: CGRect(x:UIScreen.main.bounds.width*0.02, y:UIScreen.main.bounds.height*0.1, width: UIScreen.main.bounds.width*0.96, height:UIScreen.main.bounds.height*0.94))
    
    //a header for the webView
    let closeWebViewBtn = UIButton(frame: CGRect(x: UIScreen.main.bounds.width*0.02, y: UIScreen.main.bounds.height*0.03, width: UIScreen.main.bounds.width*0.96, height: UIScreen.main.bounds.height*0.07))
    
    //using external pod M13Checkbox to create a nice checkbox
    @IBOutlet weak var checkbox: M13Checkbox!
    
    @IBOutlet weak var eulaTitleWidth: NSLayoutConstraint!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var parentButton: UIButton!
    @IBOutlet weak var childButton: UIButton!
    @IBOutlet weak var eulaButton: UIButton!
    @IBOutlet weak var termLbl: UILabel!
    @IBOutlet weak var pcTitle: UILabel!
    var attrs = [.foregroundColor: UIColor.white,
                 .underlineStyle: 1] as [NSAttributedStringKey : Any]
    var attributedString = NSMutableAttributedString(string:"")

    override func viewDidAppear(_ animated: Bool) {
        UserDefaults.standard.set(true, forKey: USER_DEF_PREFIX + USER_SEEN_ONBOARDING)
        UserDefaults.standard.synchronize()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set checkbox UI style
        checkbox._IBBoxType = M13Checkbox.BoxType.square.rawValue
        checkbox.stateChangeAnimation = M13Checkbox.Animation.bounce(M13Checkbox.AnimationStyle.fill)
        
        //set parent Button UI style
        parentButton.layer.borderColor = UIColor.white.cgColor
        parentButton.layer.borderWidth = 1
        parentButton.layer.cornerRadius = 5
        
        //set child Button UI style
        childButton.layer.borderColor = UIColor.white.cgColor
        childButton.layer.borderWidth = 1
        childButton.layer.cornerRadius = 5
        
        //open EULA webview button
        let buttonTitleStr = NSMutableAttributedString(string:NSLocalizedString("the terms", comment: "Terms of use Licence Agreement"), attributes:attrs)
        attributedString.append(buttonTitleStr)
        eulaButton.setAttributedTitle(attributedString, for: .normal)
        
        //set webview header title and UI style
        closeWebViewBtn.setTitle(NSLocalizedString("Licence Agreement", comment: "Terms of use Licence Agreement"), for: .normal)
        closeWebViewBtn.backgroundColor = UIColor.white
        closeWebViewBtn.setTitleColor(hexStringToUIColor(hex: "#094489"), for: .normal)
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = hexStringToUIColor(hex: "#094489").cgColor
        border.frame = CGRect(x: 0, y: closeWebViewBtn.frame.size.height - width, width:  closeWebViewBtn.frame.size.width, height: closeWebViewBtn.frame.size.height)
        border.borderWidth = width
        closeWebViewBtn.layer.addSublayer(border)
        let size = (closeWebViewBtn.title(for: .normal)! as NSString).size(withAttributes: [NSAttributedStringKey.font:closeWebViewBtn.titleLabel!.font]).width
        let btnSize = closeWebViewBtn.frame.width
        let arrowImageSize = #imageLiteral(resourceName: "blue_back").size.width
        let offset = ((btnSize - size - arrowImageSize) / 2 )
        
        if UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft {
            closeWebViewBtn.setImage(#imageLiteral(resourceName: "blue_next"), for: .normal)
            //adding spacing between title & arrow symbol
            closeWebViewBtn.imageEdgeInsets = UIEdgeInsetsMake(0, offset - 16, 0, -offset)
        } else {
            closeWebViewBtn.setImage(#imageLiteral(resourceName: "blue_back"), for: .normal)
            closeWebViewBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -offset + 16, 0, offset)
        }
        closeWebViewBtn.layer.masksToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(loginSignupViewController.removeSubview))
        closeWebViewBtn.isUserInteractionEnabled = true
        closeWebViewBtn.addGestureRecognizer(tap)
        
        //set webview link to keepers EULA location
        let myURL = URL(string: NSLocalizedString("https://www.bezeqint.net/cyberkids/termsofuse", comment: "Link to terms of usage in EULA"))
        let myURLRequest: URLRequest = URLRequest(url: myURL!)
        myWebView.loadRequest(myURLRequest)
    }

    @IBAction func eulaClick(_ sender: UIButton) {
        //open the webview with 0.5 sec delay
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionCurlUp,
                          animations: {self.view.addSubview(self.myWebView)}, completion: nil)
        //add the header
        self.view.addSubview(closeWebViewBtn)
    }
    
    @objc func removeSubview(gestureRecognizer: UIGestureRecognizer) {
        myWebView.removeFromSuperview() //remove webview
        closeWebViewBtn.removeFromSuperview()     //remove header
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickChild(_ sender: Any) {
        
        self.childButton.backgroundColor = UIColor.white
        self.childButton.setTitleColor(hexStringToUIColor(hex: "#094489"), for: .normal)
        //is EULA checked ?
        if (checkbox.checkState == M13Checkbox.CheckState.checked) {
            //go to parent login
            performSegue(withIdentifier: "pcToChildLogin", sender: self)
        } else {
            //EULA must! be checked.
            
            //create alert with complition handler
          
            let alert = UIAlertController(title: NSLocalizedString("Please accept the terms", comment: "Terms checkbox was not checked alert title"), message: NSLocalizedString("", comment : "Terms checkbox was not checked alert body"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment:"ok option on alerts"), style: .default) { action in
                self.childButton.backgroundColor = UIColor.clear
                self.childButton.setTitleColor(UIColor.white, for: .normal)
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onClickParent(_ sender: Any) {
        
        self.parentButton.backgroundColor = UIColor.white
        self.parentButton.setTitleColor(hexStringToUIColor(hex: "#094489"), for: .normal)
        //is EULA checked ?
        if (checkbox.checkState == M13Checkbox.CheckState.checked) {
            //show alert - parent is not part of the project
            let alert = UIAlertController(title: NSLocalizedString("Parent side is not part of my project!", comment: "Parent side is not part of my project!"), message: NSLocalizedString("", comment : "Parent side is not part of my project!"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment:"ok option on alerts"), style: .default) { action in
                self.parentButton.backgroundColor = UIColor.clear
                self.parentButton.setTitleColor(UIColor.white, for: .normal)
            })
            self.present(alert, animated: true, completion: nil)
        } else {
            //EULA must! be checked.
            
            //create alert with complition handler
            
            let alert = UIAlertController(title: NSLocalizedString("Please accept the terms", comment: "Terms checkbox was not checked alert title"), message: NSLocalizedString("", comment : "Terms checkbox was not checked alert body"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment:"ok option on alerts"), style: .default) { action in
                self.parentButton.backgroundColor = UIColor.clear
                self.parentButton.setTitleColor(UIColor.white, for: .normal)
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
}

