//
//  LocationUtils.swift
//  cyberKids-childSide
//
//  Created by Eduard Amuiev on 14/05/2018.
//  Copyright © 2018 keepers. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class LocationUtils: NSObject {
    lazy var instance =  CLLocationManager()
    
    func setupLocationManager() {
        DispatchQueue.main.async {
            self.instance.delegate = UIApplication.shared.delegate as! AppDelegate
            self.instance.distanceFilter = kCLDistanceFilterNone
            self.instance.desiredAccuracy = kCLLocationAccuracyBest
            self.instance.distanceFilter = 25
            self.instance.allowsBackgroundLocationUpdates = true
            self.instance.pausesLocationUpdatesAutomatically = false
            
            if CLLocationManager.authorizationStatus() != .authorizedAlways {
                self.instance.requestAlwaysAuthorization()
                self.instance.startMonitoringSignificantLocationChanges()
                self.instance.startUpdatingLocation()
            }
        }
    }
}
